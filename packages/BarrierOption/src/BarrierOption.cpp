#include<iostream>
#include<cmath>
#include"getOneGaussianByBoxMueller.h"
#include"BarrierOption.h"


//definition of constructor
BarrierOption::BarrierOption(
	int nInt_,
	double strike_,
	double spot_,
	double vol_,
	double r_,
	double b_,
	double expiry_){
		nInt = nInt_;
		strike = strike_;
		spot = spot_;
		vol = vol_;
		r = r_;
		b = b_;
		expiry = expiry_;
		generatePath();
}

//method definition
void BarrierOption::generatePath(){
	double thisDrift = (r * expiry - 0.5 * vol * vol * expiry) / double(nInt);
	double cumShocks = 0;
	thisPath.clear();

	for(int i = 0; i < nInt; i++){
		cumShocks += (thisDrift + vol * sqrt(expiry / double(nInt)) * getOneGaussianByBoxMueller());
		thisPath.push_back(spot * exp(cumShocks));
	}
}

//method definition

//getMaximum to check if the option has hit the maximum in a particular path
double BarrierOption::getMaximum(){

	double runningMaximum = 0.0;

	for(int i = 0; i < nInt; i++){
		if(runningMaximum<thisPath[i]){
			runningMaximum = thisPath[i];
		}
	}

	return runningMaximum;
}


//method definition
void BarrierOption::printPath(){

	for(int i = 0;  i < nInt; i++){

		std::cout << thisPath[i] << "\n";

	}

}

double BarrierOption::getPriceOfPath(){
	
	//generatePath();
	double maxi = getMaximum();
	
	double singlePrice = 0;
	
	if(maxi < b){
		double lastPrice = thisPath.back();
		if (strike-lastPrice>0){
			singlePrice = strike-lastPrice;
		}			
	}
	
	return singlePrice;
}


//method definition
double BarrierOption::getUpAndOutPutPrice(int nReps){

	double rollingSum = 0.0; //suma wyplat z opcji do pozniejszego policzenia sredniej wyplaty - EX(wyplaty)


	for(int i = 0; i < nReps; i++){
		generatePath();
		rollingSum += getPriceOfPath();
	}

	return exp(-r*expiry)*rollingSum/double(nReps);

}
