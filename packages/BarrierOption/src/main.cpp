
#include<Rcpp.h>
#include<vector>
#include<ctime>
#include<cstdlib>
#include"BarrierOption.h"
#include"getVecMean.h"
#include"getVecStdDev.h"

using namespace Rcpp;
using std::vector;

// [[Rcpp::export]]
double getUpAndOutPutPrice(
  int nInt = 126,
  double Strike = 50,
  double Spot = 45,
  double Vol = 0.25,
  double Rfr = 0.04,
  double B = 55,
  double Expiry = 0.5,
  int nReps = 10000){

	// set the seed
	srand( time(NULL) );

	// create a new instance of a class
	BarrierOption myBarrier(nInt, Strike, Spot, Vol, Rfr, B, Expiry);

	// call the method to get option price
	double price = myBarrier.getUpAndOutPutPrice(nReps);
	
	// return option price  
	return price;
}



