#include<iostream>
#include<vector>
#include<ctime>
#include<cstdlib>
#include"BarrierOption.h"
#include"getVecMean.h"
#include"getVecStdDev.h"

using std::vector;
using std::cout;
using std::cin;

int main(){

	// set the seed
	srand( time(NULL) );

	//create a new instance of class
	BarrierOption myBarrier(126, 50, 45, 0.25, 0.04, 0.5, 60);
	
	//co do cholery oznacza nInt_ ??


	// Iterate over all the elements.
	// myBarrier.printPath();

	//get arithmetic means
	cout << "arithmetic mean = " << myBarrier.getMaximum() <<"\n";

	//get last price of underlying
	cout << "Last price of underlying = " << myBarrier.thisPath.back() << "\n";
	
	cout << "Price of this one Option = " << myBarrier.getPriceOfPath() << "\n";

	//run Monte Carlo to obtain theoretical price of Asian options
	cout << "Price of arithmetic Up and Out Put Option = " << myBarrier.getUpAndOutPutPrice(10000) << "\n";

	//check whether the Data Generating Process runs correctly
	//(is the expected price and volatility of underlying close to option parameters?)
	vector<double> myVec2;
	for(int i = 0; i < 1000; i++){
		myBarrier.generatePath();
		myVec2.push_back(myBarrier.thisPath.back());
	}

	cout << "mean of last underlying prices is "   << getVecMean(myVec2)   << "\n";
	cout << "stddev of last underlying prices is " << getVecStdDev(myVec2) << "\n";

	//cout << "\nPress Enter to continue...";
	//cin.get();
	return 0;
}
