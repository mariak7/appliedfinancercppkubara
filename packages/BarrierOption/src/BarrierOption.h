#include<vector>

class BarrierOption{
public:

	//constructor
	BarrierOption(
		int nInt_,
		double strike_,
		double spot_,
		double vol_,
		double r_,
		double b_,
		double expiry_
		);

	//destructor
	~BarrierOption(){};

	//methods
	void generatePath();
	double getMaximum();
	void printPath();
	double getPriceOfPath();
	double getUpAndOutPutPrice(int nReps);
	
	//members
	std::vector<double> thisPath;
	int nInt;
	double strike;
	double spot;
	double vol;
	double r;
	double b;
	double expiry;

};
